{
    'name': 'Hotel minimenu',
    'version': '10.0.1.0.0',
    'author': 'filoquin',
    'category': 'Hotel Management',
    'website': 'https://gitlab.com/filoquin',
    'depends': ['hotel'],
    'license': 'AGPL-3',
    'summary': 'delete menu options',
    'demo': [],
    'data': [
            'data/menu.xml',
    ],
    'css': [],
    'images': [],
    'auto_install': False,
    'installable': True,
    'application': False
}